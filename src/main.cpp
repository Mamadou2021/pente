#include <iostream>

// various artificial inteligences
#include "artificialInteligence/humanAI.h"
// communication code
#include "communications/clientAI.h"
#include "communications/server.h"
// game implementation
#include "gameRules/gameLoop.h"

/*
 * no input : run with local AIs
 * ai <port> : run an ai in server mode on the given port
 * game <host:port> <host:port> : run two AI one against the other (or use localhost in the absence of further information)
 * else display help
 */
int main(int argc, char* argv[])
{
    const int nbArguments = argc - 1;
    if (nbArguments == 0)
    {
        std::cout << "Single executable mode." << std::endl;
        HumanAI player1;
        HumanAI player2;
        gameLoop(player1, player2);
    }
    else
    {
        std::string role = argv[1];
        if(role == "ai")
        {
            // parse port or use default
            int port = 31457;
            if(nbArguments > 1)
            {
                port = std::stoi(argv[2]);
            }

            // runs the AI on the given port
            std::cout << "Running Ai on port " << port << std::endl;
            HumanAI ai;
            runServer(ai, port);
        }
        else if (role == "game")
        {
            // parse ports or use default
            std::string port1 = "31457";
            std::string port2 = "31457";
            if(nbArguments > 2)
            {
                port1 = argv[2];
                port2 = argv[3];
            }

            // runs the game between the AIs on the given ports
            std::cout << "Running game between two online AIs on ports " << port1 << " and " << port2 << std::endl;
            ClientAI player1(port1);
            ClientAI player2(port2);
            gameLoop(player1, player2);
        }
        else
        {
            std::cout << "Usage:" << std::endl
                      << "Single executable mode: arbitrator.exe" << std::endl
                      << "Ai mode: arbitrator.exe ai port" << std::endl
                      << "Game mode: arbitrator.exe game port port" << std::endl;
        }
    }
}
