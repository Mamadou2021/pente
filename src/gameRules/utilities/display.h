#pragma once
#include "../types.h"
#include "../board.h"

/// functions used to display game information on screen
namespace Display
{

    /// symbol used for the player 1 stones and information
    const std::string player1Symbol = "●";
    /// symbol used for the player 2 stones and information
    const std::string player2Symbol = "○";

    /// clears the shell
    /// see: https://stackoverflow.com/a/52895729/6422174
    void clearShell()
    {
        #if defined _WIN32
        system("cls");
        #elif defined (__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
        //system("clear");
        std::cout<< u8"\033[2J\033[1;1H"; //Using ANSI Escape Sequences
        #elif defined (__APPLE__)
        system("clear");
        #endif
    }

    /// Prints informations on the state of the game in the shell
    void board(const Board& board)
    {
        // clears the shell
        clearShell();

        // displays the number of captures
        std::cout << "It is player's " << ((board.currentPlayer == Player1) ? player1Symbol : player2Symbol ) << " turn." << std::endl;
        std::cout << "Player " << player1Symbol << " captured " << (board.nbCaptures[Player1]*2) << " stones." << std::endl;
        std::cout << "Player " << player2Symbol << " captured " << (board.nbCaptures[Player2]*2) << " stones." << std::endl;

        // displays the grid
        for(unsigned int row = 0; row < gridSize; row++)
        {
            std::cout << std::setw(2) << row << ": ";
            for(unsigned int col = 0; col < gridSize; col++)
            {
                switch(board.grid[row][col])
                {
                    case Empty : std::cout << "· "; break;
                    case Player1 : std::cout << player1Symbol << ' '; break;
                    case Player2 : std::cout << player2Symbol << ' '; break;
                }
            }
            std::cout << std::endl;
        }

        // display column numbers
        std::cout << "    ";
        for(unsigned int col = 0; col < gridSize; col++)
        {
            std::cout << std::setw(2) << std::left << col;
        }
        std::cout << std::endl;
    }
}
